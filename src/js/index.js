/*jshint browser: true, jquery: true */
/*global d3 */

$(function () {
    "use strict";

    var app = window.app,
        graph,
        linkButton,
        unlinkButton,
        nodesA,
        linksA,
        nodesB,
        linksB,
        correspondence;

    nodesA = [
        {name: "A"},
        {name: "B"},
        {name: "C"},
        {name: "D"},
        {name: "E"}
    ];

    linksA = [
        {source: 0, target: 1},
        {source: 0, target: 3},
        {source: 1, target: 2},
        {source: 2, target: 3},
        {source: 3, target: 4}
    ];

    nodesB = [
        {name: "f"},
        {name: "g"},
        {name: "a"},
        {name: "h"},
        {name: "b"}
    ];

    linksB = [
        {source: 0, target: 3},
        {source: 0, target: 2},
        {source: 1, target: 2},
        {source: 1, target: 4},
        {source: 2, target: 3},
        {source: 3, target: 4}
    ];

    correspondence = {
        0: 2,
        1: 4
    };

    graph = new app.MatchedGraph({
        svg: d3.select("#graph").node(),
        nodesA: nodesA,
        linksA: linksA,
        nodesB: nodesB,
        linksB: linksB,
        nodeMatch: correspondence
    });

    graph.unlink();

    linkButton = $("#link");
    unlinkButton = $("#unlink");

    linkButton.on("click", function () {
        graph.link();

        linkButton.attr("disabled", true);
        unlinkButton.removeAttr("disabled");
    });

    unlinkButton.on("click", function () {
        graph.unlink();

        linkButton.removeAttr("disabled");
        unlinkButton.attr("disabled", true);
    });
});
