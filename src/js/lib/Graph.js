/*jshint browser: true */

(function (app, _, cola, d3) {
    "use strict";

    app.Graph = function (cfg_) {
        var cfg,
            g,
            node,
            link,
            width,
            height,
            d3cola;

        cfg = _.extend({
            svg: null,
            nodes: null,
            links: null,
            constraints: [],
            nodeColor: "skyblue",
            nodeRadius: 5,
            nodeBorder: "black",
            nodeBorderWidth: "1.5px",
            linkColor: "#999",
            linkOpacity: 0.8,
            linkWidth: 2
        }, cfg_);

        if (_.isNull(cfg.svg)) {
            throw app.error.required("svg");
        }

        if (_.isNull(cfg.nodes)) {
            throw app.error.required("nodes");
        }

        if (_.isNull(cfg.links)) {
            throw app.error.required("links");
        }

        width = d3.select(cfg.svg)
            .attr("width");

        height = d3.select(cfg.svg)
            .attr("height");

        g = d3.select(cfg.svg)
            .append("g")
            .attr("id", _.uniqueId("graph-"));

        d3cola = cola.d3adaptor()
            .nodes(cfg.nodes)
            .links(cfg.links)
            .constraints(cfg.constraints)
            .linkDistance(30)
            .size([width, height])
            .avoidOverlaps(false);

        link = g.selectAll("line.link")
            .data(cfg.links)
            .enter()
            .append("line")
            .classed("link", true)
            .style("stroke", cfg.linkColor)
            .style("stroke-opacity", cfg.linkOpacity)
            .style("stroke-width", cfg.linkWidth);

        node = g.selectAll("circle.node")
            .data(cfg.nodes)
            .enter()
            .append("circle")
            .classed("node", true)
            .attr("r", cfg.nodeRadius)
            .style("stroke", cfg.nodeBorder)
            .style("stroke-width", cfg.nodeBorderWidth)
            .style("fill", cfg.nodeColor)
            .call(d3cola.drag);

        d3cola.on("tick", function () {
            link
                .attr("x1", function (d) {
                    return d.source.x;
                })
                .attr("y1", function (d) {
                    return d.source.y;
                })
                .attr("x2", function (d) {
                    return d.target.x;
                })
                .attr("y2", function (d) {
                    return d.target.y;
                });

            node
                .attr("cx", function (d) {
                    return d.x;
                })
                .attr("cy", function (d) {
                    return d.y;
                });
        });

        d3cola.start();

        return {
            layout: d3cola,
            group: g
        };
    };

    app.MatchedGraph = function (cfg_) {
        var cfg,
            graph,
            constraints,
            nodes,
            links;

        cfg = _.extend({
            svg: null,
            nodesA: null,
            linksA: null,
            nodesB: null,
            linksB: null,
            nodeMatch: null
        }, cfg_);

        if (_.isNull(cfg.svg)) {
            throw app.error.required("svg");
        }

        if (_.isNull(cfg.nodesA)) {
            throw app.error.required("nodesA");
        }

        if (_.isNull(cfg.linksA)) {
            throw app.error.required("linksA");
        }

        if (_.isNull(cfg.nodesB)) {
            throw app.error.required("nodesB");
        }

        if (_.isNull(cfg.linksB)) {
            throw app.error.required("linksB");
        }

        if (_.isNull(cfg.nodeMatch)) {
            throw app.error.required("nodeMatch");
        }

        // Create a "master list" of nodes that records whether a node in the
        // first graph is associated to any node in the first graph (useful for
        // rendering later).
        nodes = [];
        _.each(cfg.nodesA, function (n, i) {
            var node = _.extend({}, n);
            node.matched = _.has(cfg.nodeMatch, i);

            nodes.push(node);
        });

        _.each(cfg.nodesB, function (n) {
            nodes.push(_.extend({}, n));
        });

        // Create a similar master list of links, but bump the indices in the
        // link targets for the second graph so they line up with the node
        // indices in the node master list.
        links = [];
        _.each(cfg.linksA, function (l) {
            links.push(_.extend({}, l));
        });

        _.each(cfg.linksB, function (l) {
            var link = _.extend({}, l);
            link.source += _.size(cfg.nodesA);
            link.target += _.size(cfg.nodesA);

            links.push(link);
        });

        // Finally, bump the target indices in the correspondence map, since
        // those now refer to nodes in the master list as well.
        _.each(cfg.nodeMatch, function (target, source, map) {
            map[source] += _.size(cfg.nodesA);
        });

        constraints = [];
        _.each(nodes, function (n, i) {
            var match,
                constraint;

            if (n.matched) {
                match = cfg.nodeMatch[i];

                constraint = {
                    left: i,
                    right: match,
                    gap: 1e-4,
                    equality: true
                };

                constraints.push(_.extend({
                    axis: "x"
                }, constraint));

                constraints.push(_.extend({
                    axis: "y"
                }, constraint));
            }
        });

        graph = new app.Graph({
            svg: cfg.svg,
            nodes: nodes,
            links: links,
            constraints: constraints,
            nodeColor: function (d, i) {
                return i < _.size(cfg.nodesA) ? "red" : "blue";
            },
            nodeRadius: function (d) {
                return d.matched ? 7.5 : 5;
            }
        });

        return {
            link: function () {
                graph.group.selectAll("circle.node")
                    .attr("r", function (d) {
                        return d.matched ? 7.5 : 5;
                    });

                graph.layout.constraints(constraints);
                graph.layout.start();
            },

            unlink: function () {
                graph.group.selectAll("circle.node")
                    .attr("r", 5);

                graph.layout.constraints([]);
                graph.layout.start();
            }
        };
    };
}(window.app, window._, window.cola, window.d3));
