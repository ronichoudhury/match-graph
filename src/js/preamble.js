(function () {
    "use strict";

    var app = window.app = {};

    app.error = {};
    app.error.required = function (what) {
        return new Error("option '" + what + "' is required");
    };
}());
