# Node-Matched Graphs using cola.js

An experiment in graph correspondence and [constraint-based
layout](http://marvl.infotech.monash.edu/webcola/).

## Build/Deploy Instructions

1. Install Gulp - ``npm install --global gulp``
2. Install Node dependencies - ``npm install``
3. Build the site - ``gulp``
4. Serve the site - ``cd build/site && python -m SimpleHTTPServer || python -m http.server``
5. Visit the site - ``chromium http://localhost:8000``

## Using the Demo

This demo shows a simple example of the matched-node graqph in action.  The red
and blue graphs have two of their nodes identified with each other.  To see the
correspondence explicitly, click the *Link* button - notice how the
corresponding nodes are now stuck together, forming a hybrid graph fused around
the identified nodes.  Click *Unlink* to separate the two graphs once again.
